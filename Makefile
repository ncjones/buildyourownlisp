
CC=gcc
CFLAGS=-Wall -g -std=c99
LDLIBS=-ledit -lm

parsing: parsing.c mpc/mpc.c
	$(CC) $(CFLAGS) $^ $(LDLIBS) -o $@

clean:
	rm parsing

